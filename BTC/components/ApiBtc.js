import { useState, useEffect } from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';

const url = 'http://api.coindesk.com/v1/bpi/currentprice.json';

export default function ApiBtc() {
  const [response, setResponse] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(()=> {
    fetch(url)
    .then(res => res.json())
    .then(result => {
      setIsLoading(false);
      setResponse(result);
    }, (error) => {
      setIsLoading(false);
      setError(error);
    })
  }, []);

  console.log(response)
  const getContent = () => {
    if(isLoading) {
      return (
        <View>
          <Text style={styles.textSize}>Loading Data ...</Text>
          <ActivityIndicator size="large"/> 
        </View>
      );
    }
    if(error) {
      return <Text>{error}</Text>
    }
    return(
      <View>
        <Text style={styles.textSize}>1 BTC = USD ${response["bpi"]["USD"].rate}</Text>
        <Text style={styles.textSize}>Transaction Description: {response["bpi"]["USD"].description}</Text>
        <Text style={styles.textSize}>1 BTC = EUR ${response["bpi"]["EUR"].rate}</Text>
        <Text style={styles.textSize}>1 BTC = GBP ${response["bpi"]["GBP"].rate}</Text>
      </View>
    );
  }

  return (
    <View>
      {getContent()}
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  textSize: {
    fontSize: 38,
    backgroundColor: '#741B47',
  },
});
